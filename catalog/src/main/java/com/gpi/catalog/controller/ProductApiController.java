package com.gpi.catalog.controller;

import com.gpi.catalog.dao.ProductDao;
import com.gpi.catalog.entity.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.GetMapping;

@RestController
@RequestMapping("/api/product")
public class ProductApiController {

    @Autowired
    public ProductDao productDao;
	@GetMapping("/")
    public Page<Product> findProduct(Pageable page)
    {
        return productDao.findAll(page);
    }
}
