package com.gpi.catalog.dao;

import com.gpi.catalog.entity.Product;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ProductDao extends PagingAndSortingRepository<Product,String> {
}
